# Materials
- Mobo standoffs standard 6mm tall x4
- Standoff screws x4
- Adafruit Bluefruit Feather NRF52832
- Lipo battery 1000mAh 50x32x5mm
    - Subbed in 18650 battery I had lying around, 2200mah
- 36 Switches, Diodes
- Keycaps - 30 1u, 4 1.5u, 1 1.75u, 1 1.25u
- Lots of PLA and wires

# Measurements
- Power Switch 9x13mm
- Feather 23x51mm
- Feather Mounting hole diameter 2.2mm
- Feather hole center-to-center distance on X axis 18.096mm
- Feather tallest height 7.3mm
- Approx switch clearance from bottom of plate: 6.5mm
- 18650 dimensions: 70x18.5mm
- Mobo standoff height: 6mm
- Mobo standoff thread diameter: 3.3mm

# 2022-01-25
- Reused old code from zh60 to write basic sketch, including main loop and bluetooth init
- Trying new method of immediately detecting changed key in main row loop within col loop,
    - Original code appears to gather row changed bit per col e.g 0b001111, then 
      loops through each col/row a second time to detect and handle a single key change before looping again
      maybe for debouncing purposes? Seems inefficient to me
- Also setting inputs LOW and pulling HIGH when reading, hopefully more power efficient?
- Layer method is not via bitmask, instead using a single integer (assuming we will never support two layers active at once)

# 2022-01-26
- Finished initial CAD drawing of plate and case. Case is 25mm at deepest point near top, and 15mm at shallowest
- About 7.2mm of clearance beween bottom of plate and bottom of case at the shallowest
- Without standoffs, about 8mm clearance between top of Feather and bottom of plate

# 2022-01-28
- Tested out putting together plate and case. Case too shallow angle so increased to 7.2 from 3.8 or something
  Mobo screws are too large to fit between switches. Changed up standoff locations and added special
  recessed screw part on top left of plate.
- If we don't gather all the row state before handling key changes, could we possibly get bad information about previous keyboard state?
    e.g.
    row at col 1 was 0b000011, 
    row changes to 0b000000
    if we do a one-by-one check and immediately handle a changed key
        we could continue on and handle all changed keys, then keep a scan of the entire changed row in previous state to compare on next run?

# 2022-01-30
- Built special test 2x2 grid. Found out col pins are the ones that should be connected to diodes (of course) since 
  they're the ones pulling the circuit low
- Discovered bug with keys getting stuck if Layer key is released before the key is released
- On Keyup, remove all occurences of the keyuped key
- Discovered that I was checking the previous keyboard state wrong (didn't & 1 to get the rightmost bit so of course the states were messed up)
- Suddenly windows bluetooth keyboard doesn't work? Still works on Android

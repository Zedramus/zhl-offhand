#include "Arduino.h"
#include "bluetooth.h"
#include "keycodes.h"
#include "keymap.h"

// Layers are exclusive (e.g. no pressing NUM + L1) so we can just use a single var for this
uint8_t layer = 0;

// Track which keydowns happened while layer is active
// So we can remove them all in case the Layer key was released
// before the key itself
uint8_t layer_keys[REPORT_LEN] = {0};

#define DOWN true
#define UP false

void set_layer(uint8_t l) {
    layer = l;
}

void reset_layers() {
    layer = 0;
    // Also keyup all keys in layer_keys
    for (uint8_t i = 0; i < REPORT_LEN; i++) {
        if (layer_keys[i] != 0) {
            keyup(layer_keys[i]);
        }
        layer_keys[i] = 0;
    }
}

void add_to_layer_keys(uint16_t key) {
#ifdef DEBUG
    Serial.printf("Adding %02x to layer_keys\n", key);
#endif
    for (uint8_t i = 0; i < REPORT_LEN; i++) {
        if (layer_keys[i] == key) {
            break;
        }
        if (layer_keys[i] == 0) {
            layer_keys[i] = key;
            break;
        }
    }

#ifdef DEBUG
    Serial.print("Layer keys is now ");
    for (uint8_t i = 0; i < REPORT_LEN; i++) {
        Serial.printf("%02x ", layer_keys[i]);
    }
    Serial.println("");
#endif

}

// Get keycode from keymap based on col/row triggered
uint16_t get_key(uint8_t row, uint8_t col) {
    //uint16_t key = keys[layer][row][col];
    uint16_t key = keys[layer][row][col];
    if (key != ______) {
        return key;
    }
    return K_NO;
}

void handle_keychange(uint8_t row, uint8_t col, bool state) {
    uint16_t key = get_key(row, col);
#ifdef DEBUG
    Serial.printf("Layer: %d - Key at row %d col %d: %02x ", layer, row, col, key);
    if (state) {
        Serial.println("DOWN");
    } else {
        Serial.println("UP");
    }
#endif

    if (IN_SPECIAL_FUNC_RANGE(key) && state) {
        // Handle special function instead
        switch(key) {
            case F_CLBND:
                clear_bonds();
                break;
            case F_BNDINF:
                get_conn_info();
                break;
            case F_DISC:
                disconnect();
            default:
                break;
        }
        return;
    }

    switch (key) {
        case K_L1:
        case K_L2:
        case K_LNUM:
        case K_LFN:
            {
                if (state == DOWN) {
                    // This is probably fine
                    // Get the first 4 bits of the keycode to determine layer number
                    set_layer(key & 0xF);
#ifdef DEBUG
                    Serial.printf("Layer %02x active\n", key);
#endif
                } else {
                    reset_layers();
#ifdef DEBUG
                    Serial.printf("Cleared Layer %02x\n", key);
#endif
                }
                break;
            }
        default:
            {
                if (state == DOWN) {
                    keydown(key);
                    if (layer) {
                        add_to_layer_keys(key);
                    }
                } else {
                    keyup(key);
                }
                break;
            }
    }
}

#include "core.h"
#include "battery.h"
#include "bluetooth.h"
#include "keymap.h"
#include "variant.h"
#include "wiring_digital.h"

#define COLUMNS 7
#define ROWS 6

uint8_t main_loop_interval = 8; // Main scanning loop frequency
uint16_t batcheck_interval = 60000; // check battery every 60 sec
int powersave_interval = 300000; // power save if 5 min passes without activity
uint8_t debounce_interval = 30; // Cycles to debounce

// Rows in parallel
// TODO Change 26, 25 to A6, 27 so we can free up SCL/SDA for OLED display
uint8_t rows[ROWS] = {A0, A1, A2, A3, 26, 25};
// Columns connected to diodes since they are being driven LOW when scanning the matrix
uint8_t columns[COLUMNS] = {12, 13, 14, 7, 11, 16, 15};

uint8_t prev_states[COLUMNS] = { 0 };

SoftwareTimer main_timer, bat_read_timer;

void bat_read_loop(TimerHandle_t timer) {
    read_battery();
    update_battery(vbat_per);
}

// TODO Enter interrupt mode if no keypresses after a certain time
// TODO track last keypress, and enter sleep mode with sd_power_system_off() after timeout
// How do we wake from sleep? Interrupts?
void main_loop(TimerHandle_t timer) {
    for (uint8_t c = 0; c < COLUMNS; c++) {
        //Serial.printf("COLUMN %d ::  Previous row state: %02x \n", c, prev_states[c]);
        // Row bitmask e.g. all active on that col is 0b00111111 (6 rows)
        uint8_t row_states = 0x0;

        digitalWrite(columns[c], LOW);
        //delayMicroseconds(25);

        //nrfx_coredep_delay_us(25);
        // Debounce
        for (int d = 0; d < debounce_interval; d++) { }

        for (uint8_t r = 0; r < ROWS; r++) {
            int row_val = digitalRead(rows[r]);
            if (row_val == LOW) {
                row_states |= 1 << r;
            }
            // extract previous row state, and handle a keychange if the row's current value is different
            uint8_t previous_row_state = (prev_states[c] >> r) & 1;
            //Serial.printf("\t-- r%d c%d Previous state is %x, row val is %x\n", r, c, previous_row_state, row_val);
            // Invert row_val, since 0 (ground) is considered an activated switch
            if (previous_row_state != !row_val){
#ifdef DEBUG
                Serial.printf("\t -- keychange for row %d, col %d\n", r, c);
#endif
                handle_keychange(r, c, !row_val);
            }
        }

        if (prev_states[c] != row_states) {
            prev_states[c] = row_states;
        }

        digitalWrite(columns[c], HIGH);
    }
}

void setup() {
#ifdef DEBUG
    Serial.begin(115200);
    Serial.println("Serial debug start");
#endif
    // Columns output set HIGH
    for (uint8_t i = 0; i < COLUMNS; i++){
        pinMode(columns[i], OUTPUT);
        digitalWrite(columns[i], HIGH);
    }
    // Rows are read one-by-one for each col pulled DOWN since we only have INPUT_PULLUP
    // (inputs are considered HIGH (3.3v) until pulled down)
    // I wish there was a INPUT_PULLDOWN mode
    for (uint8_t i = 0; i < ROWS; i++){
        pinMode(rows[i], INPUT_PULLUP);
        digitalWrite(rows[i], HIGH);
    }

    // Lower power mode?
    sd_power_mode_set(NRF_POWER_MODE_LOWPWR);
    sd_app_evt_wait();

    pinMode(LED_BUILTIN, OUTPUT);
    pinMode(VBAT_PIN, INPUT);
    analogReference(AR_INTERNAL_3_0);
    analogReadResolution(12);

    // Start the keyboard scanning timer
    main_timer.begin(main_loop_interval, main_loop);
    bat_read_timer.begin(batcheck_interval, bat_read_loop);

    bluetooth_init();

    main_timer.start();
    bat_read_timer.start();

    suspendLoop();
}

void loop() { } // Nothing here since we're using timers now


#include <Arduino.h>
#include "battery.h"

int vbat_raw;
uint8_t vbat_per;
float vbat_mv;

uint8_t mvToPercent(float mvolts) {
    uint8_t battery_level;

    if (mvolts >= 3000) {
        battery_level = 100;
    } else if (mvolts > 2900) {
        battery_level = 100 - ((3000 - mvolts) * 58) / 100;
    } else if (mvolts > 2740) {
        battery_level = 42 - ((2900 - mvolts) * 24) / 160;
    } else if (mvolts > 2440) {
        battery_level = 18 - ((2740 - mvolts) * 12) / 300;
    } else if (mvolts > 2100) {
        battery_level = 6 - ((2440 - mvolts) * 6) / 340;
    } else {
        battery_level = 0;
    }

    return battery_level;
}

void read_battery() {
    vbat_raw = analogRead(VBAT_PIN);
    vbat_per = mvToPercent(vbat_raw * VBAT_MV_PER_LSB);

    // FIXME: Using C-style cast.  Use static_cast<float>(...)
    // instead  [readability/casting] [4]
    // Remove [readability/casting] ignore from Makefile
    // vbat_mv = (float)vbat_raw * VBAT_MV_PER_LSB * VBAT_DIVIDER_COMP;

#ifdef DEBUG
    Serial.printf(
            "ADC = %f mV (%d) (%d%%)\n",
            vbat_raw * VBAT_MV_PER_LSB,
            vbat_raw,
            vbat_per
            );
#endif

    if (vbat_per <= 20) {
        digitalWrite(PIN_LED1, HIGH);
    } else {
        digitalWrite(PIN_LED1, LOW);
    }
}

#include <Arduino.h>
#include "core.h"
#include "bluefruit.h"
#include "bluetooth.h"
#include "keycodes.h"

// Keyboard modifiers mask
uint8_t mods = 0;
// Report keys
uint8_t report[REPORT_LEN] = {0};

BLEDis bledis;
BLEHidAdafruit blehid;
BLEBas blebas;

// Controls connection advertising
void start_advertising() {
    Bluefruit.Advertising.addFlags(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);
    Bluefruit.Advertising.addTxPower();
    Bluefruit.Advertising.addAppearance(BLE_APPEARANCE_HID_KEYBOARD);

    Bluefruit.Advertising.addService(blehid);
    Bluefruit.Advertising.addName();

    Bluefruit.Advertising.restartOnDisconnect(true);
    Bluefruit.Advertising.setInterval(32, 244);
    Bluefruit.Advertising.setFastTimeout(30);
    Bluefruit.Advertising.start(0);
}

// Initialize bluetooth service
void bluetooth_init() {
    Bluefruit.begin();
    Bluefruit.setName("ZHL - Offhand");
    Bluefruit.setTxPower(0);

    bledis.setManufacturer("ZHL");
    bledis.setModel("ZHL - Offhand NRF52");
    bledis.begin();

    blehid.begin();

#ifdef DEBUG
    Bluefruit.printInfo();
    Bluefruit.Periph.printInfo();
#endif

    // Start battery service and write 100%, until we properly report battery level
    blebas.begin();
    blebas.write(100);

    start_advertising();
}

void clear_bonds() {
    uint16_t conn_handle = Bluefruit.connHandle();
#ifdef DEBUG
    Serial.printf("Conn handle %d\n", conn_handle);
    Serial.print("Clearing all bonds... \n");
#endif
    Bluefruit.Periph.clearBonds();
}

void get_conn_info() {
    uint16_t conn_handle = Bluefruit.connHandle();
    BLEConnection * conn = Bluefruit.Connection(conn_handle);
    Serial.printf("Connection handler: %d\n", conn_handle);
    Serial.printf("Connected: %s Bonded: %s\n", conn->connected() ? "T" : "F" , conn->bonded() ? "T" : "F");
}

void disconnect() {
    uint16_t conn_handle = Bluefruit.connHandle();
    BLEConnection * conn = Bluefruit.Connection(conn_handle);
    Serial.printf("Disconnecting connection handler: %d\n", conn_handle);
    conn->disconnect();
}

void update_battery(uint8_t level) {
    blebas.notify(level);
}

// Set modifier bits
void add_mods(uint8_t newmods) {
  mods |= newmods;
}

// Unset modifier bits
void del_mods(uint8_t newmods) {
  mods &= ~newmods;
}

// Send keyboard report
void send_keyboard_report() {
    // NOTE I think this returns true/false on success/fail only if notify is enabled
    blehid.keyboardReport(mods, report);
}

void log_report() {
#ifdef DEBUG
    Serial.printf("Sending report: Mods: %02x Keys", mods);
    for (uint8_t i = 0; i < REPORT_LEN; i++) {
        Serial.printf(" %02x", report[i]);
    }
    Serial.print("\n");
#endif
}

// Trigger a keydown
void keydown(uint16_t keycode) {
    uint16_t kc = keycode;

    // Shift right 8 bits to extract only the modifier mask bits
    add_mods(kc >> 8);
    // Loop through all report key slots
    // if we find the keycode already exists do nothing
    // otherwise we add it to an empty report slot
    for (uint8_t i = 0; i < REPORT_LEN; i++) {
        // Get rightmost 8 bits by masking with 0xFF
        uint8_t key_only = (uint8_t)(kc & 0xFF);
        if (report[i] == key_only) {
            break;
        }
        // TODO probably better if we search this array to find key_only
        // Instead of trusting that the first 0 is empty
        // If we try to send duplicates keycodes in te report, that duplicate key ends up getting dropped
        if (report[i] == 0) {
            report[i] = key_only;
            break;
        }
    }

    log_report();
    send_keyboard_report();
}

// Trigger a keyup
void keyup(uint16_t keycode) {
    uint16_t kc = keycode;
    del_mods(keycode >> 8);
    // Search report slots for the keycode we want to release
    for (uint8_t i = 0; i < REPORT_LEN; i++) {
        if (report[i] == (uint8_t)(kc & 0xFF)) {
            report[i] = 0;
        }
    }

    log_report();
    send_keyboard_report();
}

# Offhand
`Always have a backup`

Left handed BT keyboard using Adafruit Bluefruit Feather NRF52832.

## Why???
I thought it would be cool to see if I could design a keyboard usable with one hand, so I can hold baby and post garbage on the internet at the same time.

I also wanted to try an ortholinear style layout.

## Requirements
- Access all (well, most) keys on a standard 104 key ANSI keyboard using left hand only
- All keys accessible with at most 2 key chords
- Common game keys in familiar enough layout to play as if on a regular QWERTY keyboard. That means WASD, QE123 etc all in expected places
- Responsive enough for games, no rollover issues (at least 6KRO)
- Wireless, so I can use it with my RPi on the couch

## Materials
- Mobo standoffs standard 6mm tall x4
- Standoff screws x4
- Adafruit Bluefruit Feather NRF52832
- 18650 battery I had lying around, 2200mah
- 36 Gateron Red Switches, 1N4148 Diodes
- Keycaps - 30 1u, 4 1.5u, 1 1.75u, 1 1.25u
- Lots of PLA and wires

## Design and key layout
![Design](images/fusion1.png)
![Design2](images/fusion2.png)
![Layout](images/offhandlayout.png)

## Completed
![complete](images/complete.jpg)


## The rats nest
![rats nest of wires](images/ratsnest.jpg)

# Setup
- Make sure BEAR and platformio is installed
- Generate compile_commands.json for completions `make clean && bear -- make all`

- check platformio detects the board `pio device list`

## Building
Run `make all`

## Uploading
`pio run -t upload` or `make upload`

`make install-and-monitor` immediately builds and flashes the firmware and starts up the serial monitor

## Serial monitor
`make monitor`

## TODO
- [x] Timer based code instead of loop()
- [ ] Interrupts for wake on power save
- [x] Figure out why keyboard dies randomly after some time
- [x] Instability? Keypresses get stuck randomly and there is a massive delay in sending key reports sometimes
	- Don't use bluetooth on onboard AX200 without the antennae!!!

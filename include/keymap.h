#include "keycodes.h"
#ifndef KEYMAP_H
#define KEYMAP_H

#define LAYERS 5
// Keyboard will have 5 layers
// 0 - Main layer mimicking left hand side of regular ANSI
// 1 - alternative letters covering the right hand side
// 2 - Arrow keys mapped to WASD, PgUp/PgDn/Home/End and some other punctuation
// 3 - NUM layer on QWERT
// 4 - F layer
#define LAYER_MAIN 0
#define LAYER_L1 1
#define LAYER_L2 2
#define LAYER_NUM 3
#define LAYER_FN 4
// 13 pins total for the matrix
#define ROWS 6
#define COLS 7

void handle_keychange(uint8_t col, uint8_t row, bool state);

const uint16_t testkeys[2][2][3] = {
    [LAYER_MAIN] = {
        /*
            Testing for now
            A B
            Shift L1
        */
        {K_A, K_B, K_C},
        {K_LSHIFT, K_L1, K_RCTRL},
    },
    [LAYER_L1] = {
        /*
            Testing for now
            H J
            Shift L1
        */
        {K_H, K_J, K_K},
        {K_LSHIFT, K_L1, K_RCTRL},
    },
};

const uint16_t keys[LAYERS][ROWS][COLS] = {
    // MAIN
    {
        /*
         * Esc  1   2   3   4     5  __
         * Tab  Q   W   E   R     T  \
         * Fn   A   S   D   F     G  __
         * LSf  Z   X   C   V     B  Enter
         * Ctrl Win Alt NUM Space __ __
         * __ __ __ __ L2 L1 Bksp
         */
        {K_ESC,    K_1,     K_2,    K_3,    K_4,    K_5,  ______},
        {K_TAB,    K_Q,     K_W,    K_E,    K_R,    K_T,  K_FSLS},
        {K_LFN,    K_A,     K_S,    K_D,    K_F,    K_G,  ______},
        {K_LSHIFT, K_Z,     K_X,    K_C,    K_V,    K_B,  K_ENT},
        {K_LCTRL,  K_LMETA, K_LEFT_ALT, ______, K_LNUM, K_SPC, ______},
        {______, ______, ______, ______,    K_L2,   K_BSPC,  K_L1},
    },

    // L1
    {
        /*
         * Esc  1   2   3   4     5  __
         * Tab  Y   U   I   O     P  \
         * Fn   H   J   K   L     ;  __
         * LSf  N   M   ,   .     /  Enter
         * Ctrl Win Alt NUM Space __ __
         * __ __ __ __ L2 L1 Del
         */
        {K_ESC,    K_1,     K_2,    K_3,    K_4,    K_5,    ______},
        {K_TAB,    K_Y,     K_U,    K_I,    K_O,    K_P,    K_FSLS},
        {K_LFN,    K_H,     K_J,    K_K,    K_L,    K_SCLN, ______},
        {K_LSHIFT, K_N,     K_M,    K_COMM, K_DOT,  K_BSLS, K_ENT},
        {K_LCTRL,  K_LMETA, K_LEFT_ALT, ______, K_LNUM, K_QUOT, ______},
        {______, ______, ______, ______,    K_L2,   K_DEL,   K_L1},
    },

    // L2
    // This layer consists of some special funcs too like the clearbond function
    {
        /*
         * CLBND  1      2    3     4     5  __
         * Tab  Home   Up   End   R     T  \
         * Fn   Left   Down Right [     ]  __
         * LSf  PgUp   PgDn -     =     B  Enter
         * Ctrl Win    Alt  NUM   Space __ __
         * __ __ __ __ L2 L1 Bksp
         */
        {F_CLBND,  F_BNDINF, F_DISC,    K_3,    K_4,    K_5,    ______},
        {K_TAB,    K_HOME,  K_UARR, K_END,  K_R,    K_T,    K_FSLS},
        {K_LFN,    K_LARR,  K_DARR, K_RARR, K_LSQB, K_RSQB, ______},
        {K_LSHIFT, K_PGUP,  K_PGDN, K_MINS, K_EQL,  K_B,    K_ENT},
        {K_LCTRL,  K_LMETA, K_LEFT_ALT, ______, K_LNUM, K_SPC, ______},
        {______, ______, ______, ______,    K_L2,   K_BSPC,   K_L1},
    },

    // NUM
    {
        /*
         * Esc  1   2   3   4     5  __
         * Tab  6   7   8   9     0  \
         * Fn   A   S   D   F     G  __
         * LSf  Z   X   C   V     B  Enter
         * Ctrl Win Alt NUM Space __ __
         * __ __ __ __ L2 L1 Bksp
         */
        {K_ESC,    K_1,     K_2,    K_3,    K_4,    K_5,    ______},
        {K_TAB,    K_6,     K_7,    K_8,    K_9,    K_0,    K_FSLS},
        {K_LFN,    K_A,     K_S,    K_D,    K_F,    K_G,    ______},
        {K_LSHIFT, K_Z,     K_X,    K_C,    K_V,    K_B,    K_ENT},
        {K_LCTRL,  K_LMETA, K_LEFT_ALT, ______, K_LNUM, K_SPC, ______},
        {______, ______, ______, ______,    K_L2,   K_BSPC,   K_L1},
    },

    // FN
    {
        /*
         * Esc  F1  F2  F3  F4    F5 F6
         * Tab  F7  F8   F9 F10  F11 F12
         * Fn   A   S   D   F     G  __
         * LSf  Z   X   C   V     B  Enter
         * Ctrl Win Alt NUM Space __ __
         * __ __ __ __ L2 L1 Bksp
         */
        {K_ESC,    K_F1,    K_F2,   K_F3,   K_F4,   K_F5,  K_F6},
        {K_TAB,    K_F7,    K_F8,   K_F9,   K_F10,  K_F11, K_F12},
        {K_LFN,    K_A,     K_S,    K_D,    K_F,    K_G,   ______},
        {K_LSHIFT, K_Z,     K_X,    K_C,    K_V,    K_B,   K_ENT},
        {K_LCTRL,  K_LMETA, K_LEFT_ALT, ______, K_LNUM, K_SPC, ______},
        {______, ______, ______, ______,    K_L2,   K_BSPC,  K_L1},
    },
};

#endif

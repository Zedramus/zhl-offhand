#ifndef BATTERY_H
#define BATTERY_H

#define VBAT_PIN (A7)
#define VBAT_MV_PER_LSB   (0.73242188F) // 3.0V ADC range and 12-bit ADC resolution = 3000mV/4096
#define VBAT_DIVIDER      (0.71275837F) // 2M + 0.806M voltage divider on VBAT = (2M / (0.806M + 2M))
#define VBAT_DIVIDER_COMP (1.403F) // Compensation factor for the VBAT divider

extern int vbat_raw;
extern uint8_t vbat_per;
extern float vbat_mv;

void read_battery();
void mv_to_percent(float mvolts);

#endif

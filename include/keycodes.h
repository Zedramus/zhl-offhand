#ifndef KEYCODES_H
#define KEYCODES_H

//#define IN_KEYBOARD_RANGE(kc) ((kc >> 16) == 0)
//#define IN_CONSUMER_RANGE(kc) ((kc >> 12) == 1)
//#define IN_SPECIAL_RANGE(kc)  ((kc >> 12) == 2)

// Letters and numbers
#define K_NO   0x0000
#define K_A    0x0004
#define K_B    0x0005
#define K_C    0x0006
#define K_D    0x0007
#define K_E    0x0008
#define K_F    0x0009
#define K_G    0x000A
#define K_H    0x000B
#define K_I    0x000C
#define K_J    0x000D
#define K_K    0x000E
#define K_L    0x000F
#define K_M    0x0010
#define K_N    0x0011
#define K_O    0x0012
#define K_P    0x0013
#define K_Q    0x0014
#define K_R    0x0015
#define K_S    0x0016
#define K_T    0x0017
#define K_U    0x0018
#define K_V    0x0019
#define K_W    0x001A
#define K_X    0x001B
#define K_Y    0x001C
#define K_Z    0x001D
#define K_1    0x001E
#define K_2    0x001F
#define K_3    0x0020
#define K_4    0x0021
#define K_5    0x0022
#define K_6    0x0023
#define K_7    0x0024
#define K_8    0x0025
#define K_9    0x0026
#define K_0    0x0027

// Operator keys
#define K_ENT  0x0028
#define K_ESC  0x0029
#define K_BSPC 0x002A
#define K_TAB  0x002B
#define K_SPC  0x002C
#define K_MINS 0x002D
#define K_EQL  0x002E

#define K_LSQB 0x002F
#define K_RSQB 0x0030
#define K_FSLS 0x0031
#define K_SCLN 0x0033
#define K_QUOT 0x0034
#define K_GRV  0x0035
#define K_COMM 0x0036
#define K_DOT  0x0037
#define K_BSLS 0x0038

// Function keys
#define K_F1   0x003A
#define K_F2   0x003B
#define K_F3   0x003C
#define K_F4   0x003D
#define K_F5   0x003E
#define K_F6   0x003F
#define K_F7   0x0040
#define K_F8   0x0041
#define K_F9   0x0042
#define K_F10  0x0043
#define K_F11  0x0044
#define K_F12  0x0045

// Special
#define K_PRNT 0x0046
#define K_SCRL 0x0047
#define K_PAUS 0x0048
#define K_INS  0x0049
#define K_HOME 0x004A
#define K_PGUP 0x004B
#define K_DEL  0x004C
#define K_END  0x004D
#define K_PGDN 0x004E

// Arrow Keys
#define K_RARR 0x004F
#define K_LARR 0x0050
#define K_DARR 0x0051
#define K_UARR 0x0052

#define K_NMLK 0x0053

// Modifiers (keys by themselves)
#define K_LCTL 0x00E0
#define K_LSFT 0x00E1
#define K_LALT 0x00E2
#define K_LWIN 0x00E3
#define K_RCTL 0x00E4
#define K_RSFT 0x00E5
#define K_RALT 0x00E6
#define K_RWIN 0x00E7

// These are special, they consist of 8 bits for the modifier bits,
// and 8 bits for the actual keycode itself
// The keycode bits get shifted out when we add the modifier bits (the first 8 bits
// in the BLEKEYBOARDCODE sequence)
#define KEY_MOD_LCTRL  0x01E0
#define KEY_MOD_LSHIFT 0x02E1
#define KEY_MOD_LALT   0x04E2
#define KEY_MOD_LMETA  0x08E3
#define KEY_MOD_RCTRL  0x10E4
#define KEY_MOD_RSHIFT 0x20E5
#define KEY_MOD_RALT   0x40E6
#define KEY_MOD_RMETA  0x80E7

#define K_LSHIFT KEY_MOD_LSHIFT
#define K_RSHIFT KEY_MOD_RSHIFT
#define K_LCTRL KEY_MOD_LCTRL
#define K_RCTRL KEY_MOD_RCTRL
#define K_LEFT_ALT KEY_MOD_LALT
#define K_RIGHT_ALT KEY_MOD_RALT
#define K_LMETA KEY_MOD_LMETA
#define K_RMETA KEY_MOD_RMETA

#define ______ 0x2000

// Special key masks
// Need to convert to the right modifier keycode when sending
#define K_L1   0x2001
#define K_L2   0x2002
#define K_LNUM 0x2003
#define K_LFN  0x2004

// Special functions
#define IN_SPECIAL_FUNC_RANGE(kc)  ((kc >> 12) == 3)
#define F_CLBND 0x3000 // Reset bluetooth bonds
#define F_BNDINF 0x3001 // Get bond info
#define F_DISC 0x3002 // Force disconnect

#endif //KEYCODES_H

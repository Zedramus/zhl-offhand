#ifndef BLUETOOTH_H
#define BLUETOOTH_H

#include "services/BLEDis.h"
#include "services/BLEHidAdafruit.h"

// BLE HID spec allows for up to 6 key reports at a time (6KRO)
#define REPORT_LEN 6

void bluetooth_init();
bool is_bluetooth_connected();
void clear_bonds();
void get_conn_info();
void disconnect();
void keydown(uint16_t keycode);
void keyup(uint16_t keycode);
void update_battery(uint8_t bat_percentage);

#endif //BLUETOOTH_H
